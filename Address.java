package main;

public class Address {
	private String streetAddress;
	private int bulidingNumber;
	public Address(String streetAddress, int bulidingNumber) {
		super();
		this.streetAddress = streetAddress;
		this.bulidingNumber = bulidingNumber;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public int getBulidingNumber() {
		return bulidingNumber;
	}
	public void setBulidingNumber(int bulidingNumber) {
		this.bulidingNumber = bulidingNumber;
	}
	@Override
	public String toString() {
		return "Address [streetAddress=" + streetAddress + ", bulidingNumber=" + bulidingNumber + "]";
	}
	

}
