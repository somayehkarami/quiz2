package main;

public class EmailUser extends User {
	static String phoneNumber;

	public EmailUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		this.phoneNumber = phoneNumber;

	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {

		if (phoneNumber.contains("0") && phoneNumber.contains("9")) {
			this.phoneNumber = phoneNumber;
		} else {
			throw new IllegalArgumentException("the Number needs to be bettween  0 and 9");

		}
		

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailUser other = (EmailUser) obj;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		return true;
	}

}
