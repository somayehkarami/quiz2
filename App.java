package main;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class App {

	

	public static void main(String[] args) {
		
		ArrayList<Message>listOfMessage = new ArrayList<Message>();
		
		
		Message list1 = new EmailMessage(
		new EmailUser("Judy", "Foster", new Address("Main Street ", 1), "a.r()b@g.com"),
		new EmailUser("Betty", "Beans", new Address("Second Street", 2), "v.r@g.com"), "This is one email");
Message list2 = new SmsMessage(new SmsUser("Judy", "Foster", new Address("Main Street ", 1), "1221236"),
		new SmsUser("Betty", "Beans", new Address("Second Street", 2), "1232313"), "This is one sms");
Message list3 = new EmailMessage(new EmailUser("Ali", "jorg", new Address("Main", 10), "a.r()b@g.com"),
		new EmailUser("Alex", " hay", new Address("Second", 18), "v.r@g.com"), "have a good day");
Message list4= new SmsMessage(new SmsUser("Anna", "htfi", new Address("Main", 8), "4387659042"),
		new SmsUser("Elsa", "ramid", new Address("Second", 9), "8906543215"), "have a good day");

		
		
		  listOfMessage.add(list1);
		
		  listOfMessage.add(list2);
		  listOfMessage.add(list3);
		  listOfMessage.add(list4);
		  for(Message item : listOfMessage) {
			  System.out.println(item);}
		  
		

	}
}