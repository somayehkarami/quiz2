package main;

import java.io.File;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendEmail implements ISendInfo{
  //  String Email;
	@Override
	public boolean validateMessage(User Sender, User receiver, String body) throws IllegalArgumentException {
		
		
		
		if (!((EmailUser) sender).getEmailAddress().matches("\"^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$\"")
				&& !((EmailUser) receiver).getEmailAddress().matches("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$") && body.isEmpty()
				&& body.contains("[^*!]")) {
				
			throw new IllegalArgumentException("the Email is not valid");
		}
		return true;

	}

	@Override
	public void sendMesage(Message message) {
		File list = new File("email.txt");
		try (PrintWriter output = new PrintWriter(list)) {
			item.forEach(text -> output.println(message));

		} catch (IOException e) {
			throw e;

		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Email == null) ? 0 : Email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SendEmail other = (SendEmail) obj;
		if (Email == null) {
			if (other.Email != null)
				return false;
		} else if (!Email.equals(other.Email))
			return false;
		return true;
	}

}
