package main;

public class SmsUser extends User {
	private   String phoneNumber;
	 public SmsUser(String firstName, String lastName, Address address , String emailAddress) {
		super(firstName, lastName, address);
		
		this.phoneNumber = phoneNumber;
		
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	
	
	public void setPhoneNumber(String phoneNumber) throws IllegalArgumentException {
		for (int i = 0; i < phoneNumber.length(); i++) {
			char c = phoneNumber.charAt(i);
			if (!Character.isDigit(c)) {
				throw new IllegalArgumentException();
			}
		}
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "SmsUser [phoneNumber=" + phoneNumber + "]";
	}
}
